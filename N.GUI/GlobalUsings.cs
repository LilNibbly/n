// ReSharper disable RedundantUsingDirective.Global
// ReSharper disable RedundantNameQualifier
#pragma warning disable IDE0001
#pragma warning disable IDE0005
global using global::System;
global using global::System.Collections.Generic;
global using global::System.Drawing;
global using global::System.IO;
global using global::System.Linq;
global using global::System.Threading;
global using global::System.Threading.Tasks;
global using global::System.Windows.Forms;