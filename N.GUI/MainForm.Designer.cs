﻿namespace N.GUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.LayoutTopButtons = new System.Windows.Forms.TableLayoutPanel();
            this.button8 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.HomeButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.LayoutClientArea = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.LayoutTitleArea = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.CustomExitButton = new System.Windows.Forms.Button();
            this.CustomMaximizeButton = new System.Windows.Forms.Button();
            this.CustomMinimizeButton = new System.Windows.Forms.Button();
            this.CustomAboutButton = new System.Windows.Forms.Button();
            this.TitleLabel = new System.Windows.Forms.Label();
            this.LayoutTopButtons.SuspendLayout();
            this.LayoutClientArea.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.LayoutTitleArea.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LayoutTopButtons
            // 
            this.LayoutTopButtons.ColumnCount = 5;
            this.LayoutTopButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.LayoutTopButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.LayoutTopButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.LayoutTopButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.LayoutTopButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.LayoutTopButtons.Controls.Add(this.button8, 4, 0);
            this.LayoutTopButtons.Controls.Add(this.button4, 3, 0);
            this.LayoutTopButtons.Controls.Add(this.button3, 2, 0);
            this.LayoutTopButtons.Controls.Add(this.HomeButton, 0, 0);
            this.LayoutTopButtons.Controls.Add(this.button2, 1, 0);
            this.LayoutTopButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutTopButtons.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.LayoutTopButtons.Location = new System.Drawing.Point(0, 0);
            this.LayoutTopButtons.Margin = new System.Windows.Forms.Padding(0);
            this.LayoutTopButtons.Name = "LayoutTopButtons";
            this.LayoutTopButtons.RowCount = 1;
            this.LayoutTopButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LayoutTopButtons.Size = new System.Drawing.Size(720, 43);
            this.LayoutTopButtons.TabIndex = 1;
            // 
            // button8
            // 
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Location = new System.Drawing.Point(579, 3);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(138, 37);
            this.button8.TabIndex = 4;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Location = new System.Drawing.Point(435, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(138, 37);
            this.button4.TabIndex = 3;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Location = new System.Drawing.Point(291, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(138, 37);
            this.button3.TabIndex = 2;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // HomeButton
            // 
            this.HomeButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HomeButton.Location = new System.Drawing.Point(3, 3);
            this.HomeButton.Name = "HomeButton";
            this.HomeButton.Size = new System.Drawing.Size(138, 37);
            this.HomeButton.TabIndex = 0;
            this.HomeButton.Text = "Home";
            this.HomeButton.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(147, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(138, 37);
            this.button2.TabIndex = 1;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // LayoutClientArea
            // 
            this.LayoutClientArea.ColumnCount = 1;
            this.LayoutClientArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LayoutClientArea.Controls.Add(this.label2, 0, 3);
            this.LayoutClientArea.Controls.Add(this.panel1, 0, 0);
            this.LayoutClientArea.Controls.Add(this.panel2, 0, 1);
            this.LayoutClientArea.Controls.Add(this.panel3, 0, 2);
            this.LayoutClientArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutClientArea.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.LayoutClientArea.Location = new System.Drawing.Point(0, 28);
            this.LayoutClientArea.Margin = new System.Windows.Forms.Padding(0);
            this.LayoutClientArea.Name = "LayoutClientArea";
            this.LayoutClientArea.RowCount = 4;
            this.LayoutClientArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.LayoutClientArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.LayoutClientArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.LayoutClientArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.LayoutClientArea.Size = new System.Drawing.Size(720, 452);
            this.LayoutClientArea.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Wheat;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(0, 431);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(720, 21);
            this.label2.TabIndex = 4;
            this.label2.Text = "Im";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PaleTurquoise;
            this.panel1.Controls.Add(this.LayoutTopButtons);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(720, 43);
            this.panel1.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 43);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(720, 43);
            this.panel2.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Salmon;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(720, 43);
            this.label1.TabIndex = 3;
            this.label1.Text = "\r\n";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SlateBlue;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 86);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(720, 345);
            this.panel3.TabIndex = 8;
            // 
            // LayoutTitleArea
            // 
            this.LayoutTitleArea.ColumnCount = 2;
            this.LayoutTitleArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LayoutTitleArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.LayoutTitleArea.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.LayoutTitleArea.Controls.Add(this.TitleLabel, 0, 0);
            this.LayoutTitleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.LayoutTitleArea.Location = new System.Drawing.Point(0, 0);
            this.LayoutTitleArea.Name = "LayoutTitleArea";
            this.LayoutTitleArea.RowCount = 1;
            this.LayoutTitleArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LayoutTitleArea.Size = new System.Drawing.Size(720, 28);
            this.LayoutTitleArea.TabIndex = 6;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Plum;
            this.flowLayoutPanel1.Controls.Add(this.CustomExitButton);
            this.flowLayoutPanel1.Controls.Add(this.CustomMaximizeButton);
            this.flowLayoutPanel1.Controls.Add(this.CustomMinimizeButton);
            this.flowLayoutPanel1.Controls.Add(this.CustomAboutButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(550, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(170, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // CustomExitButton
            // 
            this.CustomExitButton.Image = ((System.Drawing.Image)(resources.GetObject("CustomExitButton.Image")));
            this.CustomExitButton.Location = new System.Drawing.Point(131, 3);
            this.CustomExitButton.Name = "CustomExitButton";
            this.CustomExitButton.Size = new System.Drawing.Size(36, 22);
            this.CustomExitButton.TabIndex = 5;
            this.CustomExitButton.UseVisualStyleBackColor = true;
            // 
            // CustomMaximizeButton
            // 
            this.CustomMaximizeButton.Image = ((System.Drawing.Image)(resources.GetObject("CustomMaximizeButton.Image")));
            this.CustomMaximizeButton.Location = new System.Drawing.Point(89, 3);
            this.CustomMaximizeButton.Name = "CustomMaximizeButton";
            this.CustomMaximizeButton.Size = new System.Drawing.Size(36, 22);
            this.CustomMaximizeButton.TabIndex = 1;
            this.CustomMaximizeButton.UseVisualStyleBackColor = true;
            // 
            // CustomMinimizeButton
            // 
            this.CustomMinimizeButton.Image = ((System.Drawing.Image)(resources.GetObject("CustomMinimizeButton.Image")));
            this.CustomMinimizeButton.Location = new System.Drawing.Point(47, 3);
            this.CustomMinimizeButton.Name = "CustomMinimizeButton";
            this.CustomMinimizeButton.Size = new System.Drawing.Size(36, 22);
            this.CustomMinimizeButton.TabIndex = 0;
            this.CustomMinimizeButton.UseVisualStyleBackColor = true;
            // 
            // CustomAboutButton
            // 
            this.CustomAboutButton.Image = ((System.Drawing.Image)(resources.GetObject("CustomAboutButton.Image")));
            this.CustomAboutButton.Location = new System.Drawing.Point(5, 3);
            this.CustomAboutButton.Name = "CustomAboutButton";
            this.CustomAboutButton.Size = new System.Drawing.Size(36, 22);
            this.CustomAboutButton.TabIndex = 6;
            this.CustomAboutButton.UseVisualStyleBackColor = true;
            // 
            // TitleLabel
            // 
            this.TitleLabel.BackColor = System.Drawing.Color.LightPink;
            this.TitleLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TitleLabel.Location = new System.Drawing.Point(0, 0);
            this.TitleLabel.Margin = new System.Windows.Forms.Padding(0);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(550, 28);
            this.TitleLabel.TabIndex = 2;
            this.TitleLabel.Text = "Title";
            this.TitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(720, 480);
            this.ControlBox = false;
            this.Controls.Add(this.LayoutClientArea);
            this.Controls.Add(this.LayoutTitleArea);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(720, 480);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Form";
            this.LayoutTopButtons.ResumeLayout(false);
            this.LayoutClientArea.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.LayoutTitleArea.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private TableLayoutPanel LayoutTopButtons;
        private Button button4;
        private Button button3;
        private Button HomeButton;
        private Button button2;
        private TableLayoutPanel LayoutClientArea;
        private Label TitleLabel;
        private Label label2;
        private Label label1;
        private Panel panel1;
        private Button button8;
        private TableLayoutPanel LayoutTitleArea;
        private FlowLayoutPanel flowLayoutPanel1;
        private Button CustomMinimizeButton;
        private Button CustomMaximizeButton;
        private Button CustomExitButton;
        private Panel panel2;
        private Panel panel3;
        private Button CustomAboutButton;
    }
}