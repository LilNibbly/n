﻿using System.Runtime.InteropServices;

using N.Framework;
using N.Framework.Formatting;

using Timer = System.Windows.Forms.Timer;

namespace N.GUI;

public partial class MainForm : Form
{
    #region Setup Custom Border

    private void SetupEventHandlers()
    {
        this.CustomExitButton.Click += (_, _) => { Close(); };
        this.CustomMaximizeButton.Click += CustomMaximizeButton_Click;
        this.CustomMinimizeButton.Click += CustomMinimizeButton_Click;
        this.CustomAboutButton.Click += CustomAboutButton_Click;
        this.TitleLabel.MouseDown += TitleLabel_MouseDown;
    }


    private void SetupCustomBorderProperties()
    {
        this.FormBorderStyle = FormBorderStyle.Sizable;
        this.TitleLabel.Text = this.Text;
        this.Size = this.MinimumSize;
    }

    #region Window move

    // ReSharper disable all

    private const int WM_NCLBUTTONDOWN = 0xA1;
    private const int HTCAPTION = 0x2;

    [DllImport("User32.dll")]
    private static extern bool ReleaseCapture();

    [DllImport("User32.dll")]
    private static extern int SendMessage(
        IntPtr hWnd, int Msg, int wParam, int lParam);

    // ReSharper restore all

    private void TitleLabel_MouseDown(object? sender, MouseEventArgs e)
    {
        if (e.Button != MouseButtons.Left)
        {
            return;
        }

        ReleaseCapture();
        SendMessage(
            this.Handle, WM_NCLBUTTONDOWN, HTCAPTION, 0);
    }

    #endregion Window move

    #region CreateParams

    // ReSharper disable all
    /// <summary>
    /// Remove 'caption' from form but still leave form sizable,
    /// for minimal border + text in task bar.
    /// </summary>
    /// https://stackoverflow.com/a/3162167/15416963
    protected override CreateParams CreateParams
    {
        get
        {
            var @params = base.CreateParams;
            @params.Style &= ~0x00C00000; // remove WS_CAPTION
            return @params;
        }
    }
    // ReSharper restore all

    #endregion CreateParams

    #region Minimize / Maximize / About

    private void CustomAboutButton_Click(object sender, EventArgs e)
    {
        new AboutBox().ShowDialog();
    }

    private void CustomMinimizeButton_Click(object? sender, EventArgs e)
    {
        this.WindowState = FormWindowState.Minimized;
    }

    private void CustomMaximizeButton_Click(object? sender, EventArgs e)
    {
        if (this.WindowState != FormWindowState.Maximized)
        {
            Screen screen = Screen.FromHandle(this.Handle);
            this.MaximizedBounds = new Rectangle(
                new Point(-8, 0),
                screen.WorkingArea.Size + new Size(16, 10));
            this.WindowState = FormWindowState.Maximized;
        }
        else
        {
            this.WindowState = FormWindowState.Normal;
        }
    }

    #endregion Minimize / Maximize / About

    #endregion Setup Custom Border

    public MainForm()
    {
        InitializeComponent();

        this.Load += MainForm_Load;
        this.Shown += MainForm_Shown;
    }

    private void MainForm_Load(object? sender, EventArgs e)
    {
        SetupCustomBorderProperties();
    }

    private void MainForm_Shown(object? sender, EventArgs e)
    {
        SetupEventHandlers();

        timer.Interval = 100;
        timer.Tick += Timer_Tick;
        timer.Start();
    }


    #region temp
    BigDouble Gold { get; set; } = 0;
    Timer timer = new Timer();
    Random random = new Random();

    private void Timer_Tick(object sender, EventArgs e)
    {
        Gold += random.NextDouble() * 100;
        label1.Invoke(() =>
        {
            label1.Text = Gold.ToTwoLetterNotation();
        });
    }
    #endregion temp
}
