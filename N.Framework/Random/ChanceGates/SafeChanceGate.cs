﻿using JetBrains.Annotations;

using System;

namespace N.Framework.Random.ChanceGates;

/// <summary>
///     https://gamedev.stackexchange.com/q/201805
/// </summary>
/// <typeparam name="TValue"></typeparam>
[PublicAPI]
public class SafeChanceGate<TValue>
{
    private readonly ChanceContainer _chanceItem;

    public bool Roll() => this._chanceItem.Roll();

    public SafeChanceGate(TValue value,
        double chance,
        double failuresIncreaseBy,
        ulong? increaseThreshold = null,
        ulong? maxFailures = null)
    {
        this._chanceItem = new ChanceContainer
        {
            Chance = chance,
            OriginalChance = chance,
            Value = value,
            FailuresIncreaseBy = failuresIncreaseBy,
            IncreaseThreshold = (ulong)Math.Ceiling(1f / chance),
            MaxFailures = (ulong)Math.Ceiling(1f / chance * 1.5),
        };
        if (increaseThreshold.HasValue)
        {
            this._chanceItem.IncreaseThreshold = increaseThreshold.Value;
        }

        if (maxFailures.HasValue)
        {
            this._chanceItem.MaxFailures = maxFailures.Value;
        }
    }

    public ulong MissCount => this._chanceItem.MissCount;

    public TValue? Value => this._chanceItem.Value;


    private sealed class ChanceContainer
    {
        internal ChanceContainer()
        {
            this._random = new System.Random();
        }

        private readonly System.Random _random;
        public TValue? Value { get; set; }

        public double Chance { get; set; }

        public ulong MissCount { get; set; }

        public ulong IncreaseThreshold { get; set; }

        public ulong MaxFailures { get; set; }

        public double FailuresIncreaseBy { get; set; }

        public double OriginalChance { get; set; }

        public bool Roll()
        {
            if (this.MaxFailures > 0 && this.MissCount >= this.MaxFailures)
            {
                Reset(this.OriginalChance);
                return true;
            }

            if (this._random.NextDouble() <= this.Chance)
            {
                Reset(this.OriginalChance);
                return true;
            }

            this.MissCount++;
            if (this.IncreaseThreshold > 0 && this.MissCount >= this.IncreaseThreshold)
            {
                this.Chance += this.FailuresIncreaseBy;
            }

            return false;
        }

        private void Reset(double chance)
        {
            this.Chance = chance;
            this.MissCount = 0ul;
        }
    }
}