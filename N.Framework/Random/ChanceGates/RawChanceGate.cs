﻿using JetBrains.Annotations;

namespace N.Framework.Random.ChanceGates;

/// <summary>
///     https://gamedev.stackexchange.com/q/201805
/// </summary>
/// <typeparam name="TValue"></typeparam>
[PublicAPI]
public class RawChanceGate<TValue>
{
    private readonly ChanceContainer _chanceItem;


    public RawChanceGate(
        TValue value,
        double chance)
    {
        this._chanceItem = new ChanceContainer
        {
            Chance = chance,
            Value = value
        };
    }

    public ulong MissCount => this._chanceItem.MissCount;

    public TValue? Value => this._chanceItem.Value;

    public bool Roll() => this._chanceItem.Roll();

    private sealed class ChanceContainer
    {
        internal ChanceContainer()
        {
            this._random = new System.Random();
        }

        public TValue? Value { get; set; }

        public double Chance { get; set; }

        public ulong MissCount { get; private set; }

        private readonly System.Random _random;

        public bool Roll()
        {
            if (this._random.NextDouble() > this.Chance)
            {
                this.MissCount++;
                return false;
            }

            Reset();
            return true;
        }

        private void Reset()
        {
            this.MissCount = 0ul;
        }
    }
}