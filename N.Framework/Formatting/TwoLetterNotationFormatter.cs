﻿using System;
using System.Collections.Generic;

namespace N.Framework.Formatting;

public static class TwoLetterNotationFormatter
{
    private static readonly int CharA = Convert.ToInt32('a');

    private static readonly Dictionary<int, string> units = new()
    {
        { 0, "" },
        { 1, "K" },
        { 2, "M" },
        { 3, "B" },
        { 4, "T" }
    };

    public static string ToTwoLetterNotation(this double value)
    {
        var negative = value < 0d;

        value = Math.Abs(value);

        var n = (int)Math.Log(value, 1000);
        var m = value / Math.Pow(1000, n);
        string unit;

        if (n < units.Count)
        {
            unit = units[n];
        }
        else
        {
            var unitInt = n - units.Count;
            var secondUnit = unitInt % 26;
            var firstUnit = unitInt / 26;
            unit = Convert.ToChar(firstUnit + CharA) + Convert.ToChar(secondUnit + CharA).ToString();
        }

        // Math.Floor(m * 100) / 100) fixes rounding errors
        var ret = (Math.Floor(m * 100) / 100).ToString("0.##") + unit;
        if (negative)
        {
            ret = "-" + ret;
        }

        return ret;
    }

    public static string ToTwoLetterNotation(this BigDouble value)
    {
        var negative = value < 0d;

        value = BigDouble.Abs(value);

        var n = (int)BigDouble.Log(value, 1000);
        BigDouble m = value / BigDouble.Pow(1000, n);
        string unit;

        if (n < units.Count)
        {
            unit = units[n];
        }
        else
        {
            var unitInt = n - units.Count;
            var secondUnit = unitInt % 26;
            var firstUnit = unitInt / 26;
            unit = Convert.ToChar(firstUnit + CharA) + Convert.ToChar(secondUnit + CharA).ToString();
        }

        // 0.56d.ToString("0.##");
        // Math.Floor(m * 100) / 100) fixes rounding errors
        // var ret = (BigDouble.Floor(m * 100) / 100).ToString("F0") + unit;
        // var ret = BigDouble.Floor(m).ToString("F2") + unit;
        // var ret = BigDouble.Floor(m).ToString("0.##") + unit;

        // var format = value.Exponent > BigDouble.MaxSignificantDigits ? "F2" : "F0";
        // var ret = BigDouble.Floor(m).ToString(format) + unit;

        //var ret = BigDouble.Floor(m) + unit;
        var ret = m.ToString("F3") + unit;

        if (negative)
        {
            ret = "-" + ret;
        }
        return ret;
    }
}