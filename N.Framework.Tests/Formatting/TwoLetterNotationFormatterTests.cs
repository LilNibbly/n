﻿using N.Framework.Formatting;

namespace N.Framework.Tests.Formatting;

[TestFixture]
public class TwoLetterNotationFormatterTests
{
    [Test]
    public void TwoLetterNotationFormatterDoubleTest()
    {
        Assert.Multiple(() =>
        {
            Assert.That((-1_000d).ToTwoLetterNotation(), Is.EqualTo("-1K"));
            Assert.That((-1_000_000d).ToTwoLetterNotation(), Is.EqualTo("-1M"));
            Assert.That((-1_000_000_000d).ToTwoLetterNotation(), Is.EqualTo("-1B"));
            Assert.That((-1_000_000_000_000d).ToTwoLetterNotation(), Is.EqualTo("-1T"));
            Assert.That((-1_000_000_000_000_000d).ToTwoLetterNotation(), Is.EqualTo("-1aa"));
            Assert.That(double.MinValue.ToTwoLetterNotation(), Is.EqualTo("-179.76dt"));

            Assert.That(1_000d.ToTwoLetterNotation(), Is.EqualTo("1K"));
            Assert.That(1_000_000d.ToTwoLetterNotation(), Is.EqualTo("1M"));
            Assert.That(1_000_000_000d.ToTwoLetterNotation(), Is.EqualTo("1B"));
            Assert.That(1_000_000_000_000d.ToTwoLetterNotation(), Is.EqualTo("1T"));
            Assert.That(1_000_000_000_000_000d.ToTwoLetterNotation(), Is.EqualTo("1aa"));
            Assert.That(double.MaxValue.ToTwoLetterNotation(), Is.EqualTo("179.76dt"));
        });
    }

    [Test]
    public void TwoLetterNotationFormatterBigDoubleTest()
    {
        var dMinAsBd = new BigDouble(double.MinValue);
        var dMaxAsBd = new BigDouble(double.MaxValue);

        BigDouble dMinSquared = -1 * dMinAsBd.Sqr();
        BigDouble dMaxSquared = dMaxAsBd.Sqr();

        BigDouble dMinCubed = dMinAsBd.Pow(3);
        BigDouble dMaxCubed = dMaxAsBd.Pow(3);

        BigDouble dMinPow6 = -1 * dMinAsBd.Pow(6);
        BigDouble dMaxPow6 = dMaxAsBd.Pow(6);

        Assert.Multiple(() =>
        {
            Assert.That(new BigDouble(-1000).ToTwoLetterNotation(), Is.EqualTo("-1K"));
            Assert.That(new BigDouble(-1000000).ToTwoLetterNotation(), Is.EqualTo("-1M"));
            Assert.That(new BigDouble(-1000000000).ToTwoLetterNotation(), Is.EqualTo("-1B"));
            Assert.That(new BigDouble(-1000000000000).ToTwoLetterNotation(), Is.EqualTo("-1T"));
            Assert.That(new BigDouble(-1000000000000000).ToTwoLetterNotation(), Is.EqualTo("-1aa"));
            Assert.That(dMinAsBd.ToTwoLetterNotation(), Is.EqualTo("-179dt"));
            Assert.That(dMinSquared.ToTwoLetterNotation(), Is.EqualTo("-32hs"));
            Assert.That(dMinCubed.ToTwoLetterNotation(), Is.EqualTo("-5lr"));
            Assert.That(dMinPow6.ToTwoLetterNotation(), Is.EqualTo("-33xn"));

            Assert.That(new BigDouble(1000).ToTwoLetterNotation(), Is.EqualTo("1K"));
            Assert.That(new BigDouble(1000000).ToTwoLetterNotation(), Is.EqualTo("1M"));
            Assert.That(new BigDouble(1000000000).ToTwoLetterNotation(), Is.EqualTo("1B"));
            Assert.That(new BigDouble(1000000000000).ToTwoLetterNotation(), Is.EqualTo("1T"));
            Assert.That(new BigDouble(1000000000000000).ToTwoLetterNotation(), Is.EqualTo("1aa"));
            Assert.That(dMaxAsBd.ToTwoLetterNotation(), Is.EqualTo("179dt"));
            Assert.That(dMaxSquared.ToTwoLetterNotation(), Is.EqualTo("32hs"));
            Assert.That(dMaxCubed.ToTwoLetterNotation(), Is.EqualTo("5lr"));
            Assert.That(dMaxPow6.ToTwoLetterNotation(), Is.EqualTo("33xn"));
        });
    }
}