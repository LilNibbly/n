﻿using System;
using System.Collections.Generic;
using System.Linq;

using JetBrains.Annotations;

namespace N.Framework.Tests.BigDoubleTests
{
    public static class BigDoubleCompatibilityTestsHelpers
    {
        [PublicAPI]
        public class TestCaseCombinator
        {
            private readonly List<TestCaseValue> _values = new();

            public TestCaseCombinator Value(string name, double value)
            {
                this._values.Add(new TestCaseValue(name, value, BigDouble.Tolerance));
                return this;
            }

            public IEnumerable<TestCaseData> UnaryTestCases
            {
                get
                {
                    return this._values.Select(
                        v => new TestCaseData(new UnaryTestCase(v.Value, v.Precision)).SetName(v.Name));
                }
            }

            public IEnumerable<TestCaseData> BinaryTestCases
            {
                get
                {
                    var current = 0;
                    while (current < this._values.Count)
                    {
                        for (var i = current; i < this._values.Count; i++)
                        {
                            foreach (TestCaseData testCaseData in Permutate(this._values[current], this._values[i]))
                            {
                                yield return testCaseData;
                            }
                        }

                        current++;
                    }
                }
            }

            public static IEnumerable<TestCaseData> Permutate(TestCaseValue first, TestCaseValue second)
            {
                yield return TestCaseData(first, second);
                if (first != second)
                {
                    yield return TestCaseData(second, first);
                }
            }

            public static TestCaseData TestCaseData(TestCaseValue first, TestCaseValue second)
            {
                var testCase = new BinaryTestCase(first.Value, second.Value, Math.Max(first.Precision, second.Precision));
                return new TestCaseData(testCase).SetName($"{first.Name}; {second.Name}");
            }

            public class TestCaseValue
            {
                public string Name { get; }
                public double Value { get; }
                public double Precision { get; }

                public TestCaseValue(string name, double value, double precision)
                {
                    this.Name = name;
                    this.Value = value;
                    this.Precision = precision;
                }
            }
        }

        [PublicAPI]
        public class UnaryTestCase
        {
            private readonly double @double;
            private readonly BigDouble bigDouble;
            private readonly double precision;

            public UnaryTestCase(double @double, double precision = BigDouble.Tolerance)
            {
                this.@double = @double;
                this.bigDouble = @double;
                this.precision = precision;
            }

            public (double Double, BigDouble BigDouble) Values => (this.@double, this.bigDouble);

            public void AssertEqual(Func<double, double> doubleOperation,
                Func<BigDouble, BigDouble> bigDoubleOperation)
            {
                AssertEqual(doubleOperation, bigDoubleOperation, this.precision);
            }

            public void AssertEqual(Func<double, double> doubleOperation,
                Func<BigDouble, BigDouble> bigDoubleOperation, double operationPrecision)
            {
                var doubleResult = doubleOperation(this.@double);
                BigDouble bigDoubleResult = bigDoubleOperation(this.bigDouble);
                BigDoubleCompatibilityTestsHelpers.AssertEqual(bigDoubleResult, doubleResult, operationPrecision);
            }
        }

        [PublicAPI]
        public class BinaryTestCase
        {
            private readonly (double first, double second) doubles;
            private readonly (BigDouble first, BigDouble second) bigDoubles;
            private readonly double precision;

            public BinaryTestCase(double first, double second, double precision = BigDouble.Tolerance)
            {
                this.doubles = (first, second);
                this.bigDoubles = (first, second);
                this.precision = precision;
            }

            public void AssertEqual(Func<double, double, double> doubleOperation,
                Func<BigDouble, BigDouble, BigDouble> bigDoubleOperation)
            {
                var doubleResult = doubleOperation(this.doubles.first, this.doubles.second);
                BigDouble bigDoubleResult = bigDoubleOperation(this.bigDoubles.first, this.bigDoubles.second);
                BigDoubleCompatibilityTestsHelpers.AssertEqual(bigDoubleResult, doubleResult, this.precision);
            }

            public void AssertComparison(Func<double, double, bool> doubleOperation,
                Func<BigDouble, BigDouble, bool> bigDoubleOperation)
            {
                var doubleResult = doubleOperation(this.doubles.first, this.doubles.second);
                var bigDoubleResult = bigDoubleOperation(this.bigDoubles.first, this.bigDoubles.second);
                Assert.That(doubleResult, Is.EqualTo(bigDoubleResult),
                    $"Double implementation: {doubleResult}, BigDouble implementation: {bigDoubleResult}");
            }
        }

        public static readonly TestCaseCombinator GeneralTestCaseCombinator = new TestCaseCombinator()
            .Value("0", 0)
            .Value("Integer", 345)
            .Value("Negative integer", -745)
            // .Value("Big integer", 123456789)
            // .Value("Big negative integer", -987654321)
            // .Value("Small integer", 4)
            // .Value("Small negative integer", -5)
            .Value("Big value", 3.7e63)
            // .Value("Big negative value", -7.3e36)
            .Value("Really big value", 7.23e222)
            .Value("Really big negative value", -2.23e201)
            .Value("Small value", 5.323e-47)
            // .Value("Small negative value", -8.252e-21)
            .Value("Really small value", 1.98e-241)
            .Value("Really small negative value", -6.79e-215);

        public static readonly TestCaseCombinator FundamentalTestCaseCombinator = new TestCaseCombinator()
            .Value("0", 0)
            .Value("1", 1)
            .Value("-1", -1)
            .Value("1.1", 1.1)
            .Value("-1.1", -1.1)
            .Value("0.9", 0.9)
            .Value("-0.9", -0.9)
            .Value("∞", double.PositiveInfinity)
            .Value("-∞", double.NegativeInfinity)
            .Value("NaN", double.NaN);

        public static void AssertEqual(BigDouble bigDouble, double @double, double precision)
        {
            if (IsOutsideDoubleRange(bigDouble))
            {
                Assert.Ignore("Result is not in range of possible Double values");
            }

            if (BigDouble.IsNaN(bigDouble) && double.IsNaN(@double))
            {
                return;
            }
            Assert.That(bigDouble.Equals(@double, precision),
                $"Double implementation: {@double}, BigDouble implementation {bigDouble}");
        }

        public static IEnumerable<TestCaseData> FundamentalBinaryTestCases()
        {
            return FundamentalTestCaseCombinator.BinaryTestCases;
        }

        public static IEnumerable<TestCaseData> FundamentalUnaryTestCases()
        {
            return FundamentalTestCaseCombinator.UnaryTestCases;
        }

        public static IEnumerable<TestCaseData> GeneralBinaryTestCases()
        {
            return GeneralTestCaseCombinator.BinaryTestCases;
        }

        public static IEnumerable<TestCaseData> GeneralUnaryTestCases()
        {
            return GeneralTestCaseCombinator.UnaryTestCases;
        }

        public static bool IsOutsideDoubleRange(BigDouble bigDouble)
        {
            if (BigDouble.IsNaN(bigDouble) || BigDouble.IsInfinity(bigDouble))
            {
                return false;
            }

            return bigDouble.Exponent > Math.Log10(double.MaxValue)
                   || bigDouble.Exponent < Math.Log10(double.Epsilon);
        }
    }
}