using System;
using System.Text;

namespace N.Framework.Tests.BigDoubleTests;

[TestFixture]
public class BigDoubleTests
{
    private static BigDouble _testValueExponent4 = BigDouble.Parse("1.23456789e1234");
    private static BigDouble _testValueExponent1 = BigDouble.Parse("1.234567893e3");

    [Test]
    public void TestToString()
    {
        Assert.That(_testValueExponent4.ToString(), Is.EqualTo("1.23456789E+1234"));
    }

    [Test]
    public void TestToExponential()
    {
        Assert.Multiple(() =>
        {
            Assert.That(_testValueExponent4.ToString("E0"), Is.EqualTo("1E+1234"));
            Assert.That(_testValueExponent4.ToString("E4"), Is.EqualTo("1.2346E+1234"));
            Assert.That(_testValueExponent1.ToString("E0"), Is.EqualTo("1E+3"));
            Assert.That(_testValueExponent1.ToString("E4"), Is.EqualTo("1.2346E+3"));
        });
    }

    [Test]
    public void TestToFixed()
    {
        var aLotOfZeroes = new StringBuilder(1226)
            .Insert(0, "0", 1226)
            .ToString();
        Assert.Multiple(() =>
        {
            Assert.That(_testValueExponent4.ToString("F0"), Is.EqualTo("123456789" + aLotOfZeroes));
            Assert.That(_testValueExponent4.ToString("F4"), Is.EqualTo("123456789" + aLotOfZeroes + ".0000"));
            Assert.That(_testValueExponent1.ToString("F0"), Is.EqualTo("1235"));
            Assert.That(_testValueExponent1.ToString("F4"), Is.EqualTo("1234.5679"));
        });
    }

    [Test]
    public void TestEquals()
    {
        var value1 = new BigDouble(123, 5);
        var value2 = new BigDouble(1.23, 7);
        Assert.That(value1, Is.EqualTo(value2));
    }

    [Test]
    public void TestPow()
    {
        // TODO: Proper test description and test cases
        //Assert.Multiple(() =>
        //{
        var doubleResult = Math.Pow(1.1f, 5959);
        BigDouble bigDoubleResult = BigDouble.Pow(1.1f, 5959);
        //Console.WriteLine(doubleResult);
        //Console.WriteLine(bigDoubleResult);
        Assert.That(double.IsInfinity(doubleResult), Is.False);
        Assert.That(double.IsNaN(doubleResult), Is.False);
        Assert.That(BigDouble.IsInfinity(bigDoubleResult), Is.False);
        Assert.That(BigDouble.IsNaN(bigDoubleResult), Is.False);
        Assert.That(bigDoubleResult.ToDouble(), Is.EqualTo(doubleResult));

        doubleResult = Math.Pow(1.69f, 5959);
        bigDoubleResult = BigDouble.Pow(1.69f, 5959);
        Assert.That(double.IsPositiveInfinity(doubleResult), Is.True);
        Assert.That(double.IsNaN(doubleResult), Is.False);
        Assert.That(BigDouble.IsInfinity(bigDoubleResult), Is.False);
        Assert.That(BigDouble.IsNaN(bigDoubleResult), Is.False);
        Assert.That(bigDoubleResult.ToDouble(), Is.EqualTo(doubleResult));
        Console.WriteLine(doubleResult);
        Console.WriteLine(bigDoubleResult);
        Assert.That(bigDoubleResult.ToDouble(), Is.EqualTo(doubleResult));

        doubleResult = Math.Pow(-1.1f, 5959);
        bigDoubleResult = BigDouble.Pow(-1.1f, 5959);
        //Console.WriteLine(doubleResult);
        //Console.WriteLine(bigDoubleResult);
        Assert.That(double.IsNegativeInfinity(doubleResult), Is.False);
        Assert.That(double.IsNaN(doubleResult), Is.False);
        Assert.That(BigDouble.IsNegativeInfinity(bigDoubleResult), Is.False);
        Assert.That(BigDouble.IsNaN(bigDoubleResult), Is.False);
        Assert.That(bigDoubleResult.ToDouble(), Is.EqualTo(doubleResult));


        doubleResult = Math.Pow(-1.69f, 5959);
        bigDoubleResult = BigDouble.Pow(-1.69f, 5959);
        Assert.That(double.IsNegativeInfinity(doubleResult), Is.True);
        Assert.That(double.IsNaN(doubleResult), Is.False);
        Assert.That(BigDouble.IsInfinity(bigDoubleResult), Is.False);
        Assert.That(BigDouble.IsNaN(bigDoubleResult), Is.False);
        Assert.That(bigDoubleResult, Is.Not.EqualTo(BigDouble.Abs(bigDoubleResult)));
        Console.WriteLine(doubleResult);
        Console.WriteLine(bigDoubleResult);
        Assert.That(bigDoubleResult.ToDouble(), Is.EqualTo(doubleResult));
        //});
    }
}