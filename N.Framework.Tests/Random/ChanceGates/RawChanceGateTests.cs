﻿using System;
using System.Collections.Generic;
using System.Linq;

using N.Framework.Random.ChanceGates;

namespace N.Framework.Tests.Random.ChanceGates;

[TestFixture]
public class RawChanceGateTests
{
    [Test]
    public void RawChanceGateEvenChanceTest()
    {
        const int runCount = 10000;
        var maxMissed = 0ul;
        var cg = new RawChanceGate<bool>(
            true,
            0.5);
        var rolls = new List<bool>();
        for (var i = 0; i < runCount; i++)
        {
            rolls.Add(cg.Roll());
            if (cg.MissCount > maxMissed)
            {
                maxMissed = cg.MissCount;
            }
        }

        var trueCount = rolls.Count(b => b);
        var falseCount = rolls.Count(b => !b);

        Console.WriteLine($"True: {trueCount} False {falseCount}");
        Console.WriteLine($"Max Missed: {maxMissed}");

        Assert.Multiple(() =>
        {
            Assert.That(trueCount, Is.GreaterThan(0.475 * runCount));
            Assert.That(falseCount, Is.GreaterThan(0.475 * runCount));
        });
    }

    [Test]
    public void RawChanceGateHighChanceTest()
    {
        const int runCount = 10000;
        var maxMissed = 0ul;
        var cg = new RawChanceGate<bool>(
            true,
            0.8);
        var rolls = new List<bool>();
        for (var i = 0; i < runCount; i++)
        {
            rolls.Add(cg.Roll());
            if (cg.MissCount > maxMissed)
            {
                maxMissed = cg.MissCount;
            }
        }

        var trueCount = rolls.Count(b => b);
        var falseCount = rolls.Count(b => !b);

        Console.WriteLine($"True: {trueCount} False {falseCount}");

        Console.WriteLine($"Max Missed: {maxMissed}");

        Assert.Multiple(() =>
        {
            Assert.That(trueCount, Is.GreaterThan(0.75 * runCount));
            Assert.That(falseCount, Is.GreaterThan(0.15 * runCount));
        });
    }

    [Test]
    public void RawChanceGateLowChanceTest()
    {
        const int runCount = 10000;
        var maxMissed = 0ul;
        var cg = new RawChanceGate<bool>(
            true,
            0.2);
        var rolls = new List<bool>();
        for (var i = 0; i < runCount; i++)
        {
            rolls.Add(cg.Roll());
            if (cg.MissCount > maxMissed)
            {
                maxMissed = cg.MissCount;
            }
        }

        var trueCount = rolls.Count(b => b);
        var falseCount = rolls.Count(b => !b);

        Console.WriteLine($"True: {trueCount} False {falseCount}");
        Console.WriteLine($"Max Missed: {maxMissed}");

        Assert.Multiple(() =>
        {
            Assert.That(trueCount, Is.GreaterThan(0.15 * runCount));
            Assert.That(falseCount, Is.GreaterThan(0.75 * runCount));
        });
    }

    [Test]
    public void RawChanceGateValueTest()
    {
        var cg = new RawChanceGate<bool>(
            false,
            0.5);
        var value = cg.Value;

        Console.WriteLine(value.ToString());

        Assert.That(cg.Value, Is.False);
    }
}