﻿using System;
using System.Collections.Generic;
using System.Linq;

using N.Framework.Random.ChanceGates;

namespace N.Framework.Tests.Random.ChanceGates;

[TestFixture]
public class SafeChanceGateTests
{
    [Test]
    public void SafeChanceGateTrueRandomTest()
    {
        const int runCount = 10000;
        var maxMissed = 0ul;
        var cg = new SafeChanceGate<bool>(
            true,
            0.5,
            0.0,
            0,
            0);
        var rolls = new List<bool>();
        for (var i = 0; i < runCount; i++)
        {
            rolls.Add(cg.Roll());
            if (cg.MissCount > maxMissed)
            {
                maxMissed = cg.MissCount;
            }
        }

        var trueCount = rolls.Count(b => b);
        var falseCount = rolls.Count(b => !b);

        Console.WriteLine($"True: {trueCount} False {falseCount}");
        Console.WriteLine($"Max Missed: {maxMissed}");

        Assert.Multiple(() =>
        {
            Assert.That(trueCount, Is.GreaterThan(0.475 * runCount));
            Assert.That(falseCount, Is.GreaterThan(0.475 * runCount));
        });
    }

    [Test]
    public void SafeChanceGateBiasTest()
    {
        const int runCount = 10000;
        var maxMissed = 0ul;
        var cg = new SafeChanceGate<bool>(
            true,
            0.5,
            1,
            1,
            1);
        var rolls = new List<bool>();
        for (var i = 0; i < runCount; i++)
        {
            rolls.Add(cg.Roll());
            if (cg.MissCount > maxMissed)
            {
                maxMissed = cg.MissCount;
            }
        }

        var trueCount = rolls.Count(b => b);
        var falseCount = rolls.Count(b => !b);

        Console.WriteLine($"True: {trueCount} False {falseCount}");
        Console.WriteLine($"Max Missed: {maxMissed}");

        Assert.Multiple(() =>
        {
            Assert.That(trueCount, Is.GreaterThan(0.6 * runCount));
            Assert.That(falseCount, Is.GreaterThan(0.1 * runCount));
        });
    }

    [Test]
    public void SafeChanceGateBias2Test()
    {
        const int runCount = 10000;
        var maxMissed = 0ul;
        var cg = new SafeChanceGate<bool>(
            true,
            0.1,
            0.1);
        var rolls = new List<bool>();
        for (var i = 0; i < runCount; i++)
        {
            rolls.Add(cg.Roll());
            if (cg.MissCount > maxMissed)
            {
                maxMissed = cg.MissCount;
            }
        }

        var trueCount = rolls.Count(b => b);
        var falseCount = rolls.Count(b => !b);

        Console.WriteLine($"True: {trueCount} False {falseCount}");
        Console.WriteLine($"Max Missed: {maxMissed}");
    }

    [Test]
    public void SafeChanceGateHighChanceTest()
    {
        const int runCount = 10000;
        var maxMissed = 0ul;
        var cg = new SafeChanceGate<bool>(
            true,
            0.8,
            0,
            0,
            0);
        var rolls = new List<bool>();
        for (var i = 0; i < runCount; i++)
        {
            rolls.Add(cg.Roll());
            if (cg.MissCount > maxMissed)
            {
                maxMissed = cg.MissCount;
            }
        }

        var trueCount = rolls.Count(b => b);
        var falseCount = rolls.Count(b => !b);

        Console.WriteLine($"True: {trueCount} False {falseCount}");
        Console.WriteLine($"Max Missed: {maxMissed}");

        Assert.Multiple(() =>
        {
            Assert.That(trueCount, Is.GreaterThan(0.75 * runCount));
            Assert.That(falseCount, Is.GreaterThan(0.15 * runCount));
        });
    }

    [Test]
    public void SafeChanceGateLowChanceTest()
    {
        const int runCount = 10000;
        var maxMissed = 0ul;
        var cg = new SafeChanceGate<bool>(
            true,
            0.2,
            0,
            0,
            0);
        var rolls = new List<bool>();
        for (var i = 0; i < runCount; i++)
        {
            rolls.Add(cg.Roll());
            if (cg.MissCount > maxMissed)
            {
                maxMissed = cg.MissCount;
            }
        }

        var trueCount = rolls.Count(b => b);
        var falseCount = rolls.Count(b => !b);

        Console.WriteLine($"True: {trueCount} False {falseCount}");
        Console.WriteLine($"Max Missed: {maxMissed}");

        Assert.Multiple(() =>
        {
            Assert.That(trueCount, Is.GreaterThan(0.15 * runCount));
            Assert.That(falseCount, Is.GreaterThan(0.75 * runCount));
        });
    }

    [Test]
    public void SafeChanceGateValueTest()
    {
        var cg = new SafeChanceGate<bool>(
            true,
            0.5,
            0.0);
        var value = cg.Value;

        Console.WriteLine(value.ToString());

        Assert.That(cg.Value, Is.True);
    }
}